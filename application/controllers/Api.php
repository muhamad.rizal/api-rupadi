<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url','form');
        $this->load->library('form_validation');

        $this->load->model('M_user');
        $this->load->model('M_banner');
        $this->load->model('M_barang');
        $this->load->model('M_auth');
        $this->load->model('M_edukasi');
    }

    public function user()
    {
        $user = $this->M_user->user_rupadi();
        echo ($user);
    }

    public function banner()
    {
        $banner = $this->M_banner->banner_item();
        echo ($banner);
    }

    public function barang()
    {
        $barang = $this->M_barang->barang_item();
        echo ($barang);
    }

    public function kategori()
    {
        $kategori = $this->M_barang->kategori_item();
        echo ($kategori);
    }

    public function login()
    {
        $email_user = $this->input->post('email_user');
        $password = $this->input->post('password');

        $result = $this->M_auth->login_user($email_user, $password);
        echo ($result);
    }
    
    public function edukasi()
    {
        $edukasi = $this->M_edukasi->edukasi_item();
        echo ($edukasi);
    }
}