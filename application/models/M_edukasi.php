<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_edukasi extends CI_Model
{
    public function edukasi_item()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET'){
            header('Content-Type: application/json');
            echo json_encode(
                array(
                    'state' => 400, 
                    'message' => 'Bad Request'
                ));
        } else {
            $kategori = $this->db->query("SELECT * FROM `blog` ORDER BY id;");
            if ($kategori->num_rows() > 0) {
                $response = array();
                foreach ($kategori->result() as $hasil) {
                    $response[] = array(
                        'id' => $hasil->id,
                        'foto_blog' => $hasil->foto_blog,
                        'judul' => $hasil->judul,
                        'description' => $hasil->description,
                        'created_at' => $hasil->created_at,
                    );
                }
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => true,
                        'message' => 'Berhasil mendapatkan edukasi',
                        'data' => $response,
                    )
                );
            } else {
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => false,
                        'message' => 'Gagal mendapatkan edukasi',
                        'data' => null,
                    )
                );
            }
        }
    }
}