<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_auth extends CI_Model
{
    public function login_user($email_user, $password)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            header('Content-Type: application/json');
            echo json_encode(
                array(
                    'state' => 400,
                    'message' => 'Bad Request',
                ));
        } else {
            $login = $this->db->get_where('m_user', ['email_user' => $email_user])->row_array();

            if ($login) {
                if (password_verify($password, $login['password'])) {
                    $data = [
                        'id' => $login['id'],
                        'username' => $login['username'],
                        'nik' => $login['nik'],
                        'nama_lengkap' => $login['nama_lengkap'],
                        'email_user' => $login['email_user'],
                        'phone_user' => $login['phone_user'],
                        'kecamatan_kec' => $login['kecamatan_ket'],
                        'desa_ket' => $login['desa_ket'],
                        'alamat' => $login['alamat'],
                        'jenis_kelamin' => $login['jenis_kelamin'],
                        'tempat_lahir' => $login['tempat_lahir'],
                        'tanggal_lahir' => $login['tanggal_lahir'],
                    ];
                    header('Content-Type: application/json');
                    echo json_encode(
                        array(
                            'state' => true,
                            'message' => 'Berhasil login, Selamat Datang',
                            'data' => $data,
                        )
                    );
                } else {
                    header('Content-Type: application/json');
                    echo json_encode(
                        array(
                            'state' => false,
                            'message' => 'Coba lagi, Email atau password kamu salah',
                        )
                    );
                }
            } else {
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => false,
                        'message' => 'Silahkan daftar, Kamu belum terdaftar',
                    )
                );
            }
        }
    }
}
