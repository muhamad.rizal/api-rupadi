<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_barang extends CI_Model
{
    public function barang_item()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET'){
            header('Content-Type: application/json');
            echo json_encode(
                array(
                    'state' => 400, 
                    'message' => 'Bad Request'
                ));
        } else {
            $user = $this->db->query("SELECT * FROM `m_barang` ORDER BY id;");
            if ($user->num_rows() > 0) {
                $response = array();
                foreach ($user->result() as $hasil) {
                    $response[] = array(
                        'id' => $hasil->id,
                        'kode_barang' => $hasil->kode_barang,
                        'foto' => $hasil->foto,
                        'name' => $hasil->name,
                        'description' => $hasil->description,
                        'price' => $hasil->price,
                        'qty' => $hasil->qty,
                    );
                }
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => true,
                        'message' => 'Berhasil mendapatkan barang',
                        'data' => $response,
                    )
                );
            } else {
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => false,
                        'message' => 'Gagal mendapatkan barang',
                        'data' => null,
                    )
                );
            }
        }
    }

    public function detail_barang_item()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET'){
            header('Content-Type: application/json');
            echo json_encode(
                array(
                    'state' => 400, 
                    'message' => 'Bad Request'
                ));
        } else {
            $user = $this->db->query("SELECT m_barang.id, m_barang.kode_barang, m_foto.foto_brg, m_barang.name, m_barang.description, m_barang.price, m_barang.qty FROM `m_barang` INNER JOIN `m_foto` ORDER BY id;");
            if ($user->num_rows() > 0) {
                $response = array();
                foreach ($user->result() as $hasil) {
                    $response[] = array(
                        'id' => $hasil->id,
                        'kode_barang' => $hasil->kode_barang,
                        'foto_brg' => $hasil->foto_brg,
                        'name' => $hasil->name,
                        'description' => $hasil->description,
                        'price' => $hasil->price,
                        'qty' => $hasil->qty,
                    );
                }
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => true,
                        'message' => 'Berhasil mendapatkan barang',
                        'data' => $response,
                    )
                );
            } else {
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => false,
                        'message' => 'Gagal mendapatkan barang',
                        'data' => null,
                    )
                );
            }
        }
    }

    public function kategori_item()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET'){
            header('Content-Type: application/json');
            echo json_encode(
                array(
                    'state' => 400, 
                    'message' => 'Bad Request'
                ));
        } else {
            $kategori = $this->db->query("SELECT * FROM `m_kategori` ORDER BY id;");
            if ($kategori->num_rows() > 0) {
                $response = array();
                foreach ($kategori->result() as $hasil) {
                    $response[] = array(
                        'id' => $hasil->id,
                        'kategori' => $hasil->kategori,
                        'foto_kategori' => $hasil->foto_kategori,
                        'created_at' => $hasil->created_at,
                    );
                }
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => true,
                        'message' => 'Berhasil mendapatkan kategori',
                        'data' => $response,
                    )
                );
            } else {
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => false,
                        'message' => 'Gagal mendapatkan kategori',
                        'data' => null,
                    )
                );
            }
        }
    }
}
