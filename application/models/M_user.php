<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends CI_Model
{
    public function user_rupadi()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET'){
            header('Content-Type: application/json');
            echo json_encode(
                array(
                    'state' => 400, 
                    'message' => 'Bad Request'
                ));
        } else {
            $user = $this->db->get('m_user');
            if ($user->num_rows() > 0) {
                $response = array();
                foreach ($user->result() as $hasil) {
                    $response[] = array(
                        'id' => $hasil->id,
                        'username' => $hasil->username,
                        'nik' => $hasil->nik,
                        'nama_lengkap' => $hasil->nama_lengkap,
                        'email_user' => $hasil->email_user,
                        'phone_user' => $hasil->phone_user,
                        'kecamatan_ket' => $hasil->kecamatan_ket,
                        'desa_ket' => $hasil->desa_ket,
                        'alamat' => $hasil->alamat,
                        'tempat_lahir' => $hasil->tempat_lahir,
                        'tanggal_lahir' => $hasil->tanggal_lahir,
                    );
                }
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => true,
                        'message' => 'Berhasil mendapatkan user',
                        'data' => $response,
                    )
                );
            } else {
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => false,
                        'message' => 'Gagal mendapatkan user',
                        'data' => null,
                    )
                );
            }
        }
    }
}
