<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_banner extends CI_Model
{
    public function banner_item()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET'){
            header('Content-Type: application/json');
            echo json_encode(
                array(
                    'state' => 400, 
                    'message' => 'Bad Request'
                ));
        } else {
            $user = $this->db->get('m_banner');
            if ($user->num_rows() > 0) {
                $response = array();
                foreach ($user->result() as $hasil) {
                    $response[] = array(
                        'id' => $hasil->id,
                        'foto1' => $hasil->foto1,
                        'created_at' => $hasil->created_at,
                        'updated_at' => $hasil->updated_at,
                        'updated_by' => $hasil->updated_by,
                    );
                }
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => true,
                        'message' => 'Berhasil mendapatkan banner',
                        'data' => $response,
                    )
                );
            } else {
                header('Content-Type: application/json');
                echo json_encode(
                    array(
                        'state' => false,
                        'message' => 'Gagal mendapatkan banner',
                        'data' => null,
                    )
                );
            }
        }
    }
}
